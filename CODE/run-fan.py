#!/usr/bin/env python
# Original Author: Edoardo Paolo Scalafiotti <edoardo849@gmail.com>
# Modified to work on libreElec by Gary Beldon
# Modified again by WW (OSMC)

import os
import sys
import time
import RPi.GPIO as GPIO

maxTemp = 63 # The temperature in Celsius which triggers the fan
minTemp = 43 # The temperature to reach before turning the fan off
sleepTime = 30 # The delay in seconds between checks of the temperature

logfile = "/home/osmc/Scripts/Fan/fan.log" # The path to the log file
pin = 4                                    # The pin number, change if using a different pin
fanRunning = False                         # The state of the fan

def setup():
    GPIO.setwarnings(False)   # Disable warnings to the console
    GPIO.setmode(GPIO.BCM)    # BCM pin numbering, change to BOARD for absolute (P1) numbering
    GPIO.setup(pin, GPIO.OUT) # Designate pin 4 as an output (Default = Input)
    logging("Initializing.")  # Log that the script is active
    return()

def getCPUtemperature():
    res = os.popen('/opt/vc/bin/vcgencmd measure_temp').readline() # Read the Pi's temperature
    temp = float((res.replace("temp=","").replace("'C\n",""))) # Strip and convert because it comes as "temp=XX.X'C\n"
    return temp

def fanON():
    GPIO.output(pin, True)
    return()

def fanOFF():
    GPIO.output(pin, False)
    return()

def logging(message):
    logfilehandle=open(logfile,'a')
    logfilehandle.write(time.strftime("%c") + " :- " + message + "\n")
    logfilehandle.close()
    return();

def checkTemp():
    global fanRunning
    CPUtemp = float(getCPUtemperature())
    if fanRunning==False and CPUtemp>maxTemp:
        fanRunning = True
        fanON()
        logging("Temp=" + str(CPUtemp) + "C. Switching fan On.")
    if fanRunning==True and CPUtemp<minTemp:
        fanRunning = False
        fanOFF()
        logging("Temp=" + str(CPUtemp) + "C. Switching fan Off.")
    return()

try:
    setup()
    while True:
        checkTemp()
        time.sleep(sleepTime)

except KeyboardInterrupt: # Terminate after a CTRL+C keyboard interrupt
    logging("Manual Termination.")
    GPIO.cleanup() # Resets all GPIO ports used by the program

except: # Catch any other error and do the same
    logging("ERROR: an unhandled exception occurred")
    GPIO.cleanup()
