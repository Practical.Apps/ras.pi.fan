**RAS PI FAN SWITCH INSTALLATION INSTRUCTIONS**

**Summary**\
This guide will  instruct you on how to attach an in-line switch to your Raspberry Pi to control the activation of a cooling fan programatically.

**Why**\
If you have any interest in electronics as a hobby then you’ve probably heard of the Raspberry Pi as it is a very popular computer. As a small singleboard computer, the pi lacks any way to cool itself and it quickly begins to heat up. While the Pi is capable of operating over a wide temperature range, its performance suffers as it gets hotter. To keep the pi cool heat sinks are typically attached to the major chips on the board. However, passive cooling, even with very large heat sinks is not always enough. A popular use of the Pi is as a media center, streaming high definition video is an example of a task which would benefit from active cooling. Typically, active cooling on the Pi is just adding a small fan. A normal person might just connect a fan to the 5V and ground pins and be done. There are some downsides to this though. One is a lack of control over the fan, it spins as long as the Pi is on. Another is the potential for a short circuit, with plain wiring there is nothing to attenuate. This page details the wiring of a small 5V fan switch for control via GPIO, a small mod to the Raspberry Pi case to hold it, and the code needed to turn it on and off as needed.

I originally made this guide years ago. Now that I’m upgrading from a v3 Pi to a v4, I have been able to make good use of it. I decided to improve on and release it. Hopefully it will be a help to others.

![IMAGE1](IMAGES/diagrams/circuit.diagram.png "Circuit Diagram")

**Explanation**

The circuit operates with the transistor as a switch. The transistor is normally closed but when current flows from the GPIO pin to the transistor base, the circuit opens between the collector and emitter. With the transistor open, current can flow from the 5V pin through the fan to the ground pin. 

*Required components*:\
5V-0.1A Fan\
S8050 NPN transistor\
1k Ohm resistor\
1N400x diode\
Circuit board (Breadboard or perfboard)

These components are not the only ones that could be used, you can exchange them for others if you wish. For example, other components such as relays or F.E.Ts can be used as a switch in place of a regular transistor. The important thing is that they all work together. Start with the hard requirement that the fan can be at most 5V, and work from there. Worth noting is that this guide is for a two-pin fan (Power/GND) if you wish to use a three-pin you will have to figure out how to incorporate PWM on your own.

*Transistor*

Detailed information on the S8050 transistor can be found [here](http://media.nkcelectronics.com/datasheet/s8050.pdf).\
The most important characteristics are the collector current (Ic) and the current gain (hFE). The Ic maximum value of 700mA is the maximum current which should be allowed to go through the transistor (collector → emitter). Since our fan is only rated to use 100mA we are well within the limit. The trickier issue is the hFE value, which is the ratio of the Ic to the current flowing into the base. The value of hFE varies depending on other values. The sample values given suggest an hFE of around 100-120 for an Ic of 1-150mA at 1V. For our purposes, an Ic of 100mA at 5V, we should supply a base current of at least 1mA.

*Resistor*

The type of resistor used will determine that base current. Different resistors can be used depending on the current draw of the fan used. The lower the value of the resistor the more current can flow through the transistor. This will allow for a more powerful fan or another component in line with the fan, such as an LED, but be careful. While any resistor from 220 ohms to 3k ohms should work in this set-up, the S8050 transistor is rated for a maximum 700mA throughput. If you use a resistor below 520 ohms you risk allowing more current than can safely pass through the transistor. If you use a resistor below 220 ohms, you risk allowing more current than can safely pass through the GPIO pin. This is only relevant in the case of a short in the fan but it is worth considering. 1K ohms is a common resistor value and a happy medium where currents are at safe values. Using a multimeter I measured between 2-3mA on the transistor base line and 69-75mA on the collector-emitter line. The latter value did not increase with a lower value resistor so it is just a quirk of the particular kind of fan that it does not use exactly the rated 100mA. With 2mA going to the base of the transistor, it would be safe to incorporate a stronger fan rated up to 0.2A. In my upgrade I ended up using a fan rated at 0.12A

*Diode*

The function of the rectifier diode in the circuit is to protect against voltage spikes when switching the fan on and off. There is contention over whether to call it a “flyback diode” because this is not inside a TV but the premise is the same. I used the 1N4004 because they were just the kind I had available, they may be a bit overkill which is why I have it labeled as 1N400x. There is no issue with using a diode rated for higher voltages. However there are other components (like the 1N4002) which are otherwise exactly the same just weaker and potentially cheaper but still safe to use.

*Power Considerations*

Most power converters sold for powering Raspberry Pi computers are built to supply much more current than the Pi would require to run. This is to allow it to supply power for peripheral attachments, such as the fan or USB components. If you plan to use a more powerful fan, multiple fans, or power other components through your Pi at the same time, you should make sure you have a capable power supply for your needs. In my setup, the power supply is rated at a maximum 2.5A at 5V. I also have an SSD connected via a USB adapter. The SSD manufacturer’s website provides data on power consumption for different models, mine was tested to draw an absolute maximum 4.2W at 5V. A simple calculation shows that:

Power converter – Max 2.50A\
Fan – Max 0.10A\
SSD – Max 0.84A\
Available to Ras Pi – Max 1.56A

I have significantly more power available than is needed for the Pi. Especially because in my original set up I was using a model 3B, which uses very little power relative to newer models (Max 730mA). Never the less, it is important to check, especially if you intend on adding or substituting other components. The new Pi 4 models can use more than 1A. If you’d like to read more information on Raspberry Pi power consumption, see these [benchmarks](https://www.pidramble.com/wiki/benchmarks/power-consumption).

*Pins*

In this guide I will be using pins 4 (5V), 6 (Ground), and 7 (BCM4). My reason for choosing these is to use pins that are close together. There is no reason that you must use these same pins. There are multiple pins that can serve for each function. You should check https://pinout.xyz for information on what the different pins do. Important to note is the pin numbering. The absolute numbering is according to the placement of the pins on the board, there is a different numbering system corresponding to how the pins are connected to the processor chip. This will come up later in the code section.

*Diagram*

This diagram shows how to connect the circuit on a 5x5 breadboard. 

![IMAGE2](IMAGES/diagrams/breadboard.png "Breadboard diagram")\
Images sourced from the amazon product pages for the fan and mini breadboard. Pi board and component art from Fritzing.

*Case mod pictures*

I've chosen to include some actual pictures for reference. They aren't necessary though so I collapsed their references to stop from braeking up the flow of the guide.

<p>
<details>
<summary>Original breadboard case mod (Click to show)</summary>
![IMAGE3](IMAGES/pics/bboncase.png "Breadboard case")
</details>
</p>

This was my original mod with the breadboard and fan mounted to the top of the Pi 3 case.

<p>
<details>
<summary>New perfboard case mod (Click to show)</summary>
![IMAGE4](IMAGES/pics/perfboard.png "Perfboard case")
</details>
</p>

This is my new version before the perfboard was hot-glued into the inside of the Pi 4 case.

**Code**

We now come to the coding portion of the project. The code necessary for the task can be considered in two parts, the code to run and the code to allow it to run automatically. I will discuss how to do this on Open Source Media Center (OSMC) and LibreELEC (LE).

*Access*

Before you begin, consider how you will connect to your pi. In the case of OSMC, a local console login is only accessed at start-up, so it is easier to send commands via SSH from another computer. To accomplish this, SSH must be enabled. For many operating systems this is not enabled by default. In OSMC it must be enabled via `My OSMC > Network > Enable SSH`. On LE it is in `LibreELEC > Services >  Enable SSH`.
Once SSH is enabled on your system you must find the local IP address of your Raspberry Pi. In OSMC this under `Settings > System Info > Network`. If you're connected to a wireless network this address will likely be in the form 192.168.X.X, I've just used .1.11 as an example. Once this is done you can begin the SSH session by running the following command:

`ssh osmcroot@192.168.1.11` (OSMC) or `ssh root@192.168.1.11` (LE)

In OSMC, the osmc user has password ‘osmc’. Very easy to remember. In LE, the default user is the root user and the default password is ‘libreelec’. I would suggest changing these but it is not required.

*Permissions*

Permissions in a linux based operating system are important. I will discuss them in this guide when necessary but for the sake of brevity I will not go too in depth. There is only one important point to make on permissions before getting started on the guide. Many tutorials for linux related activities will preface commands with ‘#’ or ‘$’ to distinguish operations meant to be run as root or a regular user. This tutorial will not include these. On LE you are root by default, and the osmc user has sudo capability. If your operating system is different you will have to make sure you have an equivalent user to follow along.

*Requirements*

The actual program to be executed is a script written in Python. Python is almost certainly installed on whatever operating system you’re running on your Raspberry Pi. You probably even have more than one version of Python. If you would like to adapt this script into something new, especially if you want to incorporate new commands or libraries, I would suggest using Python3. The script as it is written however, executes perfectly fine using Python 2.7 (from here on simply referred to as "python"). In this guide, commands will relate to version 2.7, replace terms such as ‘pip’ or ‘python’ with ‘pip3’ or ‘python3’ if you intend to use version 3.x.

The only extra library to be installed for use in this Python script is a library called “RPi.GPIO.” There are a few different ways to get this onto your Pi. It’s possible this library already exists on your system, either because it came pre-installed with your OS or it was installed as part of a media center add on. A way to test to see if you have the library is to run this command in a terminal:

`python -c "import RPi.GPIO"`

This command may return information on its own. Such as “ImportError: No module named “RPi.GPIO.” If no output is displayed, follow immediately with the command:

`echo $?`

This should result in either “0”, the module exists, or “1” the module does not exist.

Now, this test of importing the library is not always correct. If you think you know where the library is installed on your system, you can open up a python shell and run this command before attempting to import the library:

`sys.path.append('/your/path/to/RPi.GPIO/lib')`

If you have taken a look at other similar scripts, you may have seen this line of code:

`sys.path.append('/storage/.kodi/addons/virtual.rpi-tools/lib')`

Which is a command to add the path of the RPi.GPIO library contained within the popular “Raspberry Pi Tools” add-on folder. Downloading the add-on is the easiest way to get going on LE as LibreELEC hosts the add-on in their own repo, and the LE system is so bare bones that its python does not even come with pip. That is what I did and what I suggest you do if you are using LE.
To find the add-on select the gear icon (Settings) on the main menu in Kodi. Then navigate to `Add-ons > Install from repository > LibreELEC Add-ons > Program add-ons > Raspberry Pi Tools`

Since OSMC is more of a full operating system, you have options for getting the library.

You can install it with pip using the command:

`pip install RPi.GPIO`

or install via apt

`sudo apt-get install python-rpi.gpio`

Installing via either method may require dependencies, which should be installed. For example pip may tell you that the install fails without the ‘setuptools’ module. If this is the case simply run:

`pip install setuptools`

and then try the install command for the RPi.GPIO library again.

When you receive a report that the library has been successfully installed, check the version by opening a python shell and running the commands:

`import RPi.GPIO`\
`RPi.GPIO.VERSION`

You should have a version of at least 0.6.0a3 to have the revision necessary to use the gpiomem device, for reasons that will be explained soon. If the version you installed was not recent enough, try uninstalling, updating your system, and then installing again. If your repositories were not up to date then it should be no surprise that the software package they provided you was out of date as well.

Assuming that everything worked. You should be ready to prepare the script. Provided in the `CODE` folder.

*Getting started*

The first thing to do is to find a place to save your script. This can be anywhere that your user can access it. On OSMC I created a ‘Scripts’ folder in the home directory of the default osmc user, and a subfolder for ‘Fan,’ just in case I want to start saving other scripts there. 
To save this script, navigate to the directory where you would like to save the script, e.g.:

`cd /home/osmc/Scripts/Fan`

and open up a text editor of your choice, I prefer to use nano, and make a file called run-fan.py:

`nano run-fan.py`

In the editor you can paste the text of the script. Note for those unfamiliar with nano or similar editors, CTRL+V is not the method to paste. You should right click in the terminal and select paste from the options to paste the text. Make any final adjustments that you would like while you have the editor open. Then use CTRL+X to save, ‘y’ to confirm changes, and finally ‘enter’ to confirm the file name.

The only changes really worth considering are to the temperature range. I’ve set the values as they are based on typical temperatures I recorded on my Pi. I would suggest trying the script out first before setting new values. More than anything I would recommend you not set the values equal or at all close to each other. The result will be your fan switching on and off more frequently than is really necessary. You should also consider enabling or disabling logging in the script. With the log file, you have a record to consult when making adjustments, but each log entry is a write to the disk.

The second time around on LE I opted to add the script in a folder alongside all the other add-ons and scripts. You can do this with:

`cd /storage/.kodi/addons`\
`mkdir script.fan`

Then do the same as with OSMC and create a new python file in that folder

`nano run-fan.LE.py`

Paste code into the nano editor and save. Note that the python scripts are slightly different between OSMC and LE, make sure to use the right one.

*Getting the code to run*

If you tried using RPi.GPIO or running the script already, you may have found that it does not work on your system. If you haven’t tried yet, open up a python shell, import Rpi.GPIO as GPIO, and start putting in the commands from the setup() section of the script. When you get to GPIO.setup, make sure you have either defined ‘pin’ as a number, or replace ‘pin’ with the GPIO pin number. If you enter:

`GPIO.setup(pin, GPIO.OUT)`

And receive the error “No access to /dev/mem.” Your user is not set up to access the GPIO pins. You will unfortunately have to read this condensed section to fix it. This is only likely to occur if you are not the root user on your system. It is not surprising that my second time around on LE, I did not have this issue. 

<p>
<details>
<summary>Condensed troubleshooting (Click to expand):</summary>

If, as I mentioned previously, your RPi.GPIO library is up to date then it should attempt to access /dev/gpiomem first. If your user does not have access to /dev/gpiomem, it will attempt to use /dev/mem instead. If your user also cannot access /dev/mem, then the code fails to execute. 

Restricting access to certain files is a core part of security in linux and should not be treated lightly. It is much more secure to take the extra time and have permissions set correctly than to run things as root or to open up permissions to everyone. 

![IMAGE5](IMAGES/screens/screen2.png "GPIOmeme")

With that being said, here is an example to show that opening up the permissions on /dev/gpiomem to allow everyone read/write access does allow RPi.GPIO to run when it could not before.

*Access to /dev/gpiomem*

The /dev/mem node handles addressing to all memory on the machine, it is dangerous to allow non-root access to this device. That is why /dev/gpiomem is available, to allow access to the GPIO memory space only. Some operating systems will already have /dev/gpiomem set up with access allowed to a group called ‘gpio’ which automatically includes the default user. This is not set up on OSMC but this is the preferred setting to accomplish our goal so I will show you how to create these settings on your system.

Before beginning to make changes, the permissions for the file can be checked with the command:

`ls -l /dev/gpiomem`

If the permissions do not already include a reference to ‘gpio’ you will need to follow these steps.

First, check whether a gpio group already exists by checking your group list, this can be done with:

`grep gpio /etc/group`

If the gpio group does not exist, no output will be returned.

To add the group use the command:

`sudo groupadd gpio`

Then add your user (replace user_name with the user name e.g. osmc) to that group with the command:

`sudo usermod -a -G gpio user_name`

You can check that this works by running the grep command again and checking the output.

To allow the gpio group access to /dev/gpiomem, you must change the group ownership with:

`sudo chown root.gpio /dev/gpiomem`

and allow the group read/write access with:

`sudo chmod g+rw /dev/gpiomem`

If everything went successfully then the output of another permissions check on /dev/gpiomem should look as it does at the end of this picture:

![IMAGE6](IMAGES/screens/screen3.png "Changed permissions")

Keep in mind that group changes to your user require you to log out and in again to take effect. Once you’ve exited the SSH session and reconnected, you should find that you are able to run the script and otherwise use RPi.GPIO commands without the errors. You can test this by running the command:

`python /path/to/script/run-fan.py`

If the script runs properly, you will not get a new line in your console with user@host:~$ because the command has not terminated since the script runs in an infinite loop. Once you have seen the fan turn on/off and are satisfied that the script works, you can end it by pressing CTRL+C in the terminal. For those who are not experienced with the command line, CTRL+C is the code to stop most programs that are still in-process.

There is only one problem with this setup. The /dev/gpiomem file does not persist after restart. It is generated at start-up and because of this your changes to its permissions were not permanent. Do not worry, there is a way to make those same changes automatically so that /dev/gpiomem is ready for your user when your script runs, and that’s with a udev rule.

*udev rule*

Udev rules are stored in separate text files in your udev directory. You can look at them by running:

`ls /etc/udev/rules.d`

All they are is a series of configuration text files with instructions for the operating system to execute following start-up. In my case, there was already a rule relating to the gpio. This is important to check because the order udev rules are started does matter. Udev rules can be named anything that ends with “.rules,” but they generally start with a number and the number is used to determine the order the rules run. Since my existing gpio rule begins with 996, I chose to make my rule start with ‘997’ so that it runs after. The name you choose to give it after the number does not matter.

To create the rule, run the command:

`sudo nano /etc/udev/rules.d/997-gpio-group.rules`

Then paste in the following content:

```
KERNEL=="gpiomem", NAME="%k", GROUP="gpio", MODE="0660"
```

And repeat the saving and closing procedure for nano discussed earlier.\
This line in the udev rule will make the same changes to /dev/gpiomem that were made manually before. That is, changing group ownership to the gpio group then giving the group read and write access. The creation of the gpio group and the user’s membership in the group are both persistent changes that do not need to be made again.

For more information about writing udev rules, read [this](http://www.reactivated.net/writing_udev_rules.html)

![IMAGE7](IMAGES/screens/screen7.png "UDEV rules")

Nothing wrong with checking, as I did in the screenshot, that the new rule has the same permissions set as the existing rules. If they do not match there is a chance that the rule will not run. Make sure you are root or invoking sudo when using nano in this instance.

</details>
</p>

If you don’t receive any error you can continue and run:

`GPIO.output(pin, True)`

Your fan should start, and you can stop it again with the same command replacing True with False.
With proof that your fan circuit is working, you can move on to the last step.

**Automatic starting**

The final piece to the project is getting the script to start automatically when the system turns on. While there are many ways to achieve this, I will stick to one for each system. For OSMC I will describe creating a systemd unit file (I will refer to it from here on as a ‘service’). For LibreELEC I will show how to use a startup script.

<p>
<details>
<summary> OSMC - Systemd service (Click to expand):</summary>

Systemd is still somewhat new and controversial. I will avoid any discussion of systemd alternatives except to say that if you are using an OS without systemd on your Pi, you probably already know how to call a script at startup without this part of the guide.

Similar to the udev rules, systemd services are also configuration text files. Their purpose is to give instructions to the systemd service manager on when and how to run certain commands. Our service will be instructions to start the run-fan.py script and to restart it if it gets stopped or fails for some reason. Systemd services are typically stored in /lib/systemd/system/. We will follow basically the same procedure for the udev rules, exchanging one path for the other. Naming is not as important here (as long as the file ends in .service) so name your service whatever you like, I chose “run-fan.service.”

`sudo nano /lib/systemd/system/run-fan.service`

With the editor open, paste following code (remember to use right-click instead of ctrl+v):

```
[Unit]
Description=Control a fan via GPIO based on temperature.
After=meadiacenter.service

[Service]
User=osmc
Group=gpio
Type=simple
ExecStart=/usr/bin/python /home/osmc/Scripts/Fan/run-fan.py
Restart=always
RestartSec=20

[Install]
WantedBy=multi-user.target
```

There is not much to explain about the service code as it is pretty plain to read. The only things which may be different are the path to the run-fan.py file, which you have probably saved to a different path. Note that it is a separate path from that of the python binary, so if you wish to use python3 or have python installed differently, you should change that first path as well. Also, the “mediacenter.service” is the service which controls Kodi. If you are not using a media center operating system such as OSMC, you probably do not have Kodi installed or set to start automatically. If that’s the case you can remove this line or change it to the service that controls your processor-intensive software.

If you are interested in more information on writing systemd services, go [here](https://www.freedesktop.org/software/systemd/man/systemd.unit.html)

*Enabling the service*

Just a few more steps are needed before we’re finished. First, update your systemd configuration with the command:

`sudo systemctl daemon-reload`

You should run this command again if you later decide to edit the service file.

To set the service up to run, use the command:

`sudo systemctl enable run-fan.service`

If you gave your service a different name, make sure to switch it in here.

Finally, reboot your system:

`sudo reboot`

Once your system has restarted, you can check on the status of your service with the command:

`sudo systemctl status run-fan.service`

If everything went correctly, you should have an output similar to this

![IMAGE8](IMAGES/screens/screen8.png "service status")

If there’s something wrong, you can check for errors in your journal with the command:

`sudo journalctl -u run-fan.service`

And that’s the end.

</details>
</p>

You could use systemd on LibreELEC as well but LE has a convenient feature where a shell script can be run automatic at startup. The following section outlines how to use that to achieve the same result.

<p>
<details>
<summary> LE - autostart.sh (Click to expand):</summary>

The last step is to create the startup script. All that has to be done on LE is make a shell script with the right name in the config folder. For startup it's autostart.sh, there's also an equivalent shutdown.sh to run before the pi shutsdown.

Simply create the file as with the python script

`nano /storage/.config/autostart.sh`

Then paste in the following content

```
(
 sleep 20
 python /storage/.kodi/addons/script.fan/run-fan.LE.py
)&
```

The delay and the ampersand to run the commands in the background are suggested by LE in [this wiki article](https://wiki.libreelec.tv/configuration/startup-shutdown) because the autostart script runs early in the  startup process. You want the commands to be able to run and not block other startup commands. The delay time could be made shorter if you want, since it comes from an example where kodi must already be started, but it's up to your personal preference.

</details>
</p>

Once the automatic start to your script is set up that should be it. Now every time you start the pi your script will be be managing your fan all by itself.

<p>
<details>
<summary> Hearing ringing? (Click to expand):</summary>

I noticed with the new Pi 4 that the switch circuit started making a high pitched ringing noise when connected. This only happens while the GPIO pin (7) going to the base of the transistor is set as an input. The GPIO pins are all set as inputs by default at startup. Rather than listening to the ring while you wait the 20 or so seconds for the script to hit the setup block, you can configure pin 7 as an output at start. This is done with the `config.txt` file which is in the boot folder on any pi operating system. On older versions of LE, this is in the `/SYSTEM` folder but in the latest version and likely in future versions it is in `/LIBREELEC`. You will probably have to eject and mount the SD card on another computer to access this directory as it may not be accessible to you if you are interfacing via SSH. I believe you can place the following command anywhere in the file but I chose to include it at the end.

```
# Set GPIO7 to be an output set to 0
gpio=7=op,dl
```

This sets pin 7 as an output (op) and sets the current low (dl, high would mean the fan is on). This should stop the ringing sound as soon as possible and makes it basically unnoticeable. You can find more information about this kind of setting [here](https://www.raspberrypi.org/documentation/configuration/config-txt/gpio.md).

</details>
</p>
